#ifndef __FTLIP_H__
#define __FTLIP_H__

#include <iostream>
#include <map>
#include <fstream>
#include <cerrno>
#include <algorithm>
#include <any>

using namespace std;

class ftl_value final
{
public:
    ftl_value(string value = {}) :
        m_value(move(value)){}

    bool has_value() const
    {
        return !m_value.empty();
    }

    int to_int() const
    {
        return stoi(m_value);
    }

    bool to_bool() const
    {
        if (m_value == "true" || m_value == "True" || m_value == "1")
            return true;
        else
            return false;
    }

    string to_string() const
    {
        return m_value;
    }

private:
    string m_value;
};

class ftlip
{
public:
    ftlip(string file)
    {
        ifstream if_file;
        if_file.open(move(file), ifstream::in);

        if (if_file.is_open())
        {
            string line;
            int line_num = 0;

            while(getline(if_file, line))
            {
                line_num += 1;

                size_t delimeter = line.find('=');

                if (delimeter == -1)
                    cerr << "Error tokenize on line " << line_num << "\n";

                m_data[trim(line.substr(0, delimeter))] = trim(line.substr(delimeter+1, line.length()));
            }
        }
    }

    ftl_value value(const string &field)
    {
        if (m_data.find(field) != m_data.end())
            return ftl_value(m_data[field]);

        return ftl_value();
    }

private:
    // https://stackoverflow.com/questions/216823/how-to-trim-a-stdstring#217605
    inline static string trim(const string &s)
    {
        auto wsfront = find_if_not(s.begin(),s.end(),[](int c){
            return isspace(c);
        });

        auto wsback = find_if_not(s.rbegin(),s.rend(),[](int c){
            return isspace(c);
        }).base();

        return (wsback<=wsfront ? string() : string(wsfront,wsback));
    }

    map<string, string> m_data;
};

#endif /* __FTLIP_H__ */
