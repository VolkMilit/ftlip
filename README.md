# FTLIP
*Faster Then Light* INI parser

Just a little ini-inspired parser for my little projects. Yeah, I'm bad at naming things...

#### Sample ini file

``` ini
field1 = thisthing
field2 = thisthing2
field3 = thisthing3
```

#### Sample usage

``` cpp
ftlip ini("path/to/ini/file.ini");
const std::string field1 = ini.get("field1"); // return thisthing
const std::string field4 = ini.get("field4"); // return an empty string, there is no such value
```
